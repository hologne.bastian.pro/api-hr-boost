-- Ce simple fichier avec cette convention de nommage permet de générer la base h2 au lancement. Placer dans : src/main/resources
DROP TABLE IF EXISTS employees;
DROP TABLE IF EXISTS sites;

CREATE TABLE sites (
                       id INT AUTO_INCREMENT  PRIMARY KEY,
                       name VARCHAR(250) NOT NULL,
                       country enum ('FR','GB','AR','CH','DE','ES'),
                       city VARCHAR(250) NOT NULL,
                       zip_code VARCHAR(250) NOT NULL,
                       image VARCHAR(250)
);


INSERT INTO sites (name, country, city,zip_code,image) VALUES
                                                           ('TechCenter', 'FR', 'Bordeaux','33000','HR1'),
                                                           ('Commercial', 'FR', 'Paris 01 Louvre','75001','IT1'),
                                                           ('Production', 'GB', 'Marywell','AB1', 'HR2'),
                                                           ('Towers of La Rochelle', 'FR', 'La Rochelle', '17000', 'IT4');
CREATE TABLE employees (
  id INT AUTO_INCREMENT  PRIMARY KEY,
  first_name VARCHAR(250) NOT NULL,
  last_name VARCHAR(250) NOT NULL,
  mail VARCHAR(250) NOT NULL,
  site_id INT,
  FOREIGN KEY (site_id) REFERENCES sites(id)
);

INSERT INTO employees (first_name, last_name, mail,site_id ) VALUES
                                                                 ('Laurent', 'GINA', 'laurentgina@mail.com',1),
                                                                 ('Sophie', 'FONCEK', 'sophiefoncek@mail.com',2),
                                                                 ('Agathe', 'FEELING', 'agathefeeling@mail.com',3),
                                                                 ('Alice', 'Dupont', 'alice.dupont@example.com', 1),
                                                                 ('Bob', 'Martin', 'bob.martin@example.com', 2),
                                                                 ('Charles', 'Durand', 'charles.durand@example.com', 1),
                                                                 ('David', 'Lefebvre', 'david.lefebvre@example.com', 3),
                                                                 ('Emily', 'Moreau', 'emily.moreau@example.com', 2),
                                                                 ('Frank', 'Simon', 'frank.simon@example.com', 1),
                                                                 ('Gabrielle', 'Garcia', 'gabrielle.garcia@example.com', 3),
                                                                 ('Henry', 'Fournier', 'henry.fournier@example.com', 1),
                                                                 ('Isabelle', 'Chevalier', 'isabelle.chevalier@example.com', 4),
                                                                 ('John', 'Smith', 'john.smith@example.com', 4),
                                                                 ('Kevin', 'Peterson', 'kevin.peterson@example.com', 4),
                                                                 ('Lena', 'Müller', 'lena.mueller@example.com', 2);







