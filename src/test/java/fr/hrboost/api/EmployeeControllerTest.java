package fr.hrboost.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import fr.hrboost.api.model.Employee;
import fr.hrboost.api.repository.EmployeeRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.CoreMatchers.is;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc(addFilters = false) //Permet de desactiver spring security
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class EmployeeControllerTest {

    @Autowired
    public MockMvc mockMvc;

    @Autowired
    private EmployeeRepository employeeRepository;

    @Test
    public void testGetEmployees() throws Exception {

        mockMvc.perform(get("/employees?page=0&size=10"))
                .andExpect(status().isOk())
//                .andExpect(jsonPath("content.$[0].firstName", is("Laurent")))
                .andExpect(jsonPath("content[1].firstName", is("Alice")))
                .andExpect(jsonPath("content[1].lastName", is("Dupont")))
                .andExpect(jsonPath("content[1].mail", is("alice.dupont@example.com")))

                .andExpect(jsonPath("content[2].firstName", is("Bob")))
                .andExpect(jsonPath("content[2].lastName", is("Martin")))
                .andExpect(jsonPath("content[2].mail", is("bob.martin@example.com")))

                .andExpect(jsonPath("content[0].firstName", is("Agathe")))
                .andExpect(jsonPath("content[0].lastName", is("FEELING")))
                .andExpect(jsonPath("content[0].mail", is("agathefeeling@mail.com")));
    }

    @Test
    public void testGetEmployee() throws Exception {
        Employee employee = Employee.EmployeeBuilder.anEmployee()
                .withFirstName("EmployeeTestGetEmployee")
                .withLastName("TestGetEmployee")
                .withMail("junitGetEmployee@test.com")
                .build();
        employeeRepository.save(employee);

        mockMvc.perform(get("/employees/"+employee.getId()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.firstName", is("EmployeeTestGetEmployee")))
                .andExpect(jsonPath("$.lastName", is("TestGetEmployee")))
                .andExpect(jsonPath("$.mail", is("junitGetEmployee@test.com")));
    }

    @Test
    public void testPostEmployee() throws Exception {
        Employee employee = Employee.EmployeeBuilder.anEmployee()
                .withFirstName("EmployeeTestPOST")
                .withLastName("POST")
                .withMail("junitPOST@test.com")
                .build();

        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
        String requestJson = ow.writeValueAsString(employee);

        mockMvc.perform(post("/employees")
                        .contentType(APPLICATION_JSON)
                        .content(requestJson))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.firstName", is("EmployeeTestPOST")))
                .andExpect(jsonPath("$.lastName", is("POST")))
                .andExpect(jsonPath("$.mail", is("junitPOST@test.com")));
    }

    @Test
    public void testPutEmployee() throws Exception {
        Employee employee = Employee.EmployeeBuilder.anEmployee()
                .withFirstName("EmployeeTestPUT")
                .withLastName("Test")
                .withMail("junitPUT@test.com")
                .withSiteId(1L)
                .build();
        employeeRepository.save(employee);

        employee.setFirstName("EmployeeTestPUTEDITED");
        employee.setLastName("TestEDITED");
        employee.setMail("junit@editedtest.com");
        employee.setSiteId(2L);

        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
        String requestJson = ow.writeValueAsString(employee);

        mockMvc.perform(put("/employees/" + employee.getId())
                        .contentType(APPLICATION_JSON)
                        .content(requestJson))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.firstName", is("EmployeeTestPUTEDITED")))
                .andExpect(jsonPath("$.lastName", is("TestEDITED")))
                .andExpect(jsonPath("$.mail", is("junit@editedtest.com")))
                .andExpect(jsonPath("$.siteId", is(2)));
    }

    @Test
    public void testDeleteEmployee() throws Exception {
        Employee employee = Employee.EmployeeBuilder.anEmployee()
                .withFirstName("EmployeeTestDELETE")
                .withLastName("TestDELETE")
                .withMail("junitDELETE@test.com")
                .build();
        employeeRepository.save(employee);

        mockMvc.perform(delete("/employees/" + employee.getId()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", is("Employee delete")));
    }
}
