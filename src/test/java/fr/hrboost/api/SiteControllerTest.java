package fr.hrboost.api;

import static org.hamcrest.CoreMatchers.is;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import fr.hrboost.api.model.Country;
import fr.hrboost.api.model.Site;
import fr.hrboost.api.repository.SiteRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.web.servlet.MockMvc;


@SpringBootTest
@AutoConfigureMockMvc(addFilters = false) //Permet de desactiver spring security
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class SiteControllerTest {

    @Autowired
    public MockMvc mockMvc;

    @Autowired
    private SiteRepository siteRepository;

    @Test
    public void testGetSites() throws Exception {
        mockMvc.perform(get("/sites"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].name", is("TechCenter")))
                .andExpect(jsonPath("$[0].country", is("FR")))
                .andExpect(jsonPath("$[0].city", is("Bordeaux")))
                .andExpect(jsonPath("$[0].zipCode", is("33000")))
                .andExpect(jsonPath("$[1].name", is("Commercial")))
                .andExpect(jsonPath("$[1].country", is("FR")))
                .andExpect(jsonPath("$[1].city", is("Paris 01 Louvre")))
                .andExpect(jsonPath("$[1].zipCode", is("75001")))
                .andExpect(jsonPath("$[2].name", is("Production")))
                .andExpect(jsonPath("$[2].country", is("GB")))
                .andExpect(jsonPath("$[2].city", is("Marywell")))
                .andExpect(jsonPath("$[2].zipCode", is("AB1")));
    }

    @Test
    public void testGetSite() throws Exception {
        Site site = new Site();
        site.setName("SiteTestGetSite");
        site.setCountry(Country.FR);
        site.setCity("Paris");
        site.setZipCode("75000");
        siteRepository.save(site);

        mockMvc.perform(get("/sites/" + site.getId()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name", is("SiteTestGetSite")))
                .andExpect(jsonPath("$.country", is("FR")))
                .andExpect(jsonPath("$.city", is("Paris")))
                .andExpect(jsonPath("$.zipCode", is("75000")));
    }

    @Test
    public void testCreateSite() throws Exception {
        Site site = new Site();
        site.setName("SiteTestCreateSite");
        site.setCountry(Country.GB);
        site.setCity("London");
        site.setZipCode("SW1A 1AA");

        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
        String requestJson = ow.writeValueAsString(site);

        mockMvc.perform(post("/sites")
                        .contentType(APPLICATION_JSON)
                        .content(requestJson))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.name", is("SiteTestCreateSite")))
                .andExpect(jsonPath("$.country", is("GB")))
                .andExpect(jsonPath("$.city", is("London")))
                .andExpect(jsonPath("$.zipCode", is("SW1A 1AA")));
    }

    @Test
    public void testDeleteSite() throws Exception {
        Site site = new Site();
        site.setName("SiteTestDeleteSite");
        site.setCountry(Country.FR);
        site.setCity("Lyon");
        site.setZipCode("69000");
        siteRepository.save(site);

        mockMvc.perform(delete("/sites/" + site.getId()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", is("Site deleted")));
    }

    @Test
    public void testUpdateSite() throws Exception {
        Site site = new Site();
        site.setName("SiteTestUpdateSite");
        site.setCountry(Country.FR);
        site.setCity("Toulouse");
        site.setZipCode("31000");
        siteRepository.save(site);

        site.setName("SiteTestUpdateSiteEDITED");
        site.setCity("Paris");
        site.setZipCode("75000");

        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
        String requestJson = ow.writeValueAsString(site);

        mockMvc.perform(put("/sites/" + site.getId())
                        .contentType(APPLICATION_JSON)
                        .content(requestJson))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name", is("SiteTestUpdateSiteEDITED")))
                .andExpect(jsonPath("$.country", is("FR")));
        //TODO: decommenter les lignes en dessous quand Lucas aura dev son truc (jsp quoi)
//                .andExpect(jsonPath("$.city", is("Paris")))
//                .andExpect(jsonPath("$.zipCode", is("75000")));
    }
}
