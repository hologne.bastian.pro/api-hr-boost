package fr.hrboost.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import fr.hrboost.api.model.Employee;
import fr.hrboost.api.model.jwt.JwtRequest;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc()
public class AuthenticationTest
{

    @Autowired
    public MockMvc mockMvc;
    @Test
    public void whenCredentialIsWrong_ThenApiResponse401() throws Exception {
        JwtRequest jwtRequest = new JwtRequest("wrong","wrong");
        //Conversion json via jackson
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
        String requestJson=ow.writeValueAsString(jwtRequest );


        mockMvc.perform(post("/authenticate").contentType(APPLICATION_JSON).content(requestJson))
                .andExpect(status().isUnauthorized());

    }

    @Test
    public void whenCredentialIsGood_ThenApiResponseToken() throws Exception {
        JwtRequest jwtRequest = new JwtRequest("springuser","password");
        //Conversion json via jackson
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
        String requestJson=ow.writeValueAsString(jwtRequest );

        mockMvc.perform(post("/authenticate").contentType(APPLICATION_JSON).content(requestJson))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.token").exists());

    }
}
