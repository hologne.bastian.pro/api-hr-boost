package fr.hrboost.api.controller;


import fr.hrboost.api.controller.mapper.EmployeeMapper;
import fr.hrboost.api.controller.readmodel.EmployeeFilterType;
import fr.hrboost.api.controller.readmodel.EmployeeRM;
import fr.hrboost.api.controller.readmodel.SortType;
import fr.hrboost.api.model.Employee;
import fr.hrboost.api.service.EmployeeService;
import fr.hrboost.api.service.EmployeeServiceImpl;
import fr.hrboost.api.service.SlackService;
import fr.hrboost.api.service.SlackServiceImpl;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import javassist.tools.web.BadHttpRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.Optional;


@RestController
@RequestMapping("/employees")
public class EmployeeController {

    private final EmployeeService employeeService;

    private final SlackService slackService;

    public EmployeeController(EmployeeServiceImpl employeeServiceImpl, SlackServiceImpl slackServiceImpl) {
        this.employeeService = employeeServiceImpl;
        this.slackService = slackServiceImpl;
    }


    /**
     * Create - Add a new employee
     *
     * @param employee An object employee
     * @return The employee object saved
     */
    @Operation(summary = "Create a new employee")
    @PostMapping
    public ResponseEntity<Employee> createEmployee(@Parameter(description = "The employee object") @RequestBody Employee employee) {
        Employee createdEmployee = employeeService.saveEmployee(employee);
        slackService.sendWelcomeMessage(employee);
        return new ResponseEntity<>(createdEmployee, HttpStatus.CREATED);
    }


    /**
     * Read - Get one employee
     *
     * @param id The id of the employee
     * @return An Employee object fulfilled
     */
    @Operation(summary = "Get one employee")
    @GetMapping("/{id}")
    public ResponseEntity<EmployeeRM> getEmployee(@Parameter(description = "The employee id") @PathVariable("id") final Long id) {

        Employee employee = employeeService.getEmployee(id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Unable to find resource"));

        return new ResponseEntity<>(EmployeeMapper.toReadMdel(employee), HttpStatus.OK);
    }

    /**
     * Read - Get all employees
     *
     * @return - An Iterable object of Employee fulfilled
     */
    @Operation(summary = "Get all employees")
    @GetMapping("")
    public ResponseEntity<Page<EmployeeRM>> getEmployees(@Parameter(description = "The page number") @RequestParam(name = "page") Integer page,
                                                         @Parameter(description = "The number of elements per page") @RequestParam(name = "size") Integer size,
                                                         @Parameter(description = "The sort type") @RequestParam(name = "sort", required = false) SortType sort,
                                                         @Parameter(description = "The filter type") @RequestParam(name = "filterType", required = false) EmployeeFilterType employeeFilterType,
                                                         @Parameter(description = "The filter value") @RequestParam(name = "filterValue", required = false) String filterValue) {

        Pageable sortedByName;

        //Gestion du tri ascedant ou descendant
        if (Optional.ofNullable(sort).orElse(SortType.ASC) == SortType.DESC) {
            sortedByName = PageRequest.of(page, size, Sort.by("firstName").descending());
        } else {
            sortedByName = PageRequest.of(page, size, Sort.by("firstName"));
        }


        //Gestion des filtres
        Page<Employee> employees;
        if (Optional.ofNullable(employeeFilterType).isPresent() && Optional.ofNullable(filterValue).isPresent() && employeeFilterType == EmployeeFilterType.SITE) {
            long value;

            try {
                value = Long.parseLong(filterValue);
            } catch (Exception e) {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "The filterValue must be an Integer for this filterType", e);
            }

            employees = employeeService.getEmployeesBySite(value, sortedByName);

        } else {
            employees = employeeService.getEmployees(sortedByName);
        }

        Page<EmployeeRM> employeesRM = employees.map(EmployeeMapper::toReadMdel);

        return new ResponseEntity<>(employeesRM, HttpStatus.OK);
    }

    /**
     * Update - Update an existing employee
     *
     * @param id       - The id of the employee to update
     * @param employee - The employee object updated
     * @return - The employee object updated
     */
    @Operation(summary = "Update an employee")
    @PutMapping("/{id}")
    public ResponseEntity<Employee> updateEmployee(@Parameter(description = "The id of the employee to update") @PathVariable("id") final Long id,
                                                   @Parameter(description = "The employee object updated") @RequestBody Employee employee) throws BadHttpRequest {
        Optional<Employee> e = employeeService.getEmployee(id);
        if (e.isPresent()) {
            Employee currentEmployee = e.get();

            String firstName = employee.getFirstName();
            if (firstName != null) {
                currentEmployee.setFirstName(firstName);
            }
            String lastName = employee.getLastName();
            if (lastName != null) {
                currentEmployee.setLastName(lastName);
            }
            String mail = employee.getMail();
            if (mail != null) {
                currentEmployee.setMail(mail);
            }
            Long siteId = employee.getSiteId();
            if (siteId != null) {
                currentEmployee.setSiteId(siteId);
            }

            employeeService.saveEmployee(currentEmployee);
            return new ResponseEntity<>(currentEmployee, HttpStatus.OK);
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Unable to find resource");
        }
    }

    /**
     * Delete - Delete an employee
     *
     * @param id - The id of the employee to delete
     * @return Message to valid deletion
     */
    @Operation(summary = "Delete an employee")
    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteEmployee(@Parameter(description = "The id of the employee to delete") @PathVariable("id") final Long id) {
        Optional<Employee> employee = employeeService.getEmployee(id);
        if (!employee.isPresent()) {
            return new ResponseEntity<>("Employee not found", HttpStatus.NOT_FOUND);
        } else {
            employeeService.deleteEmployee(id);
            slackService.sendDismissalMessage(employee.get());
        }
        return new ResponseEntity<>("Employee delete", HttpStatus.OK);
    }

}
