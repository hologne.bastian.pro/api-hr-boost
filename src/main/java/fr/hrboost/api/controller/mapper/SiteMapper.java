package fr.hrboost.api.controller.mapper;

import fr.hrboost.api.controller.readmodel.SiteRM;
import fr.hrboost.api.model.Site;
import fr.hrboost.api.service.ImageService;
import fr.hrboost.api.service.ImageServiceImpl;

public class SiteMapper {

    public static SiteRM toReadMdel(Site site) {

        ImageService imageService = new ImageServiceImpl();

        return SiteRM.SiteRMBuilder.aSiteRM()
                .withId(site.getId())
                .withCity(site.getCity())
                .withName(site.getName())
                .withZipCode(site.getZipCode())
                .withCountry(site.getCountry())
                .withImage(imageService.getImage(site.getImage()))
                .build();
    }
}
