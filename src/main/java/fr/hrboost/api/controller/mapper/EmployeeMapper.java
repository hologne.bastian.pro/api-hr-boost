package fr.hrboost.api.controller.mapper;

import fr.hrboost.api.controller.readmodel.EmployeeRM;
import fr.hrboost.api.model.Employee;
import fr.hrboost.api.model.Site;
import fr.hrboost.api.service.SiteService;
import fr.hrboost.api.service.SiteServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class EmployeeMapper {


    private static SiteService siteService;

    @Autowired
    public EmployeeMapper(SiteServiceImpl siteServiceImpl) {
        EmployeeMapper.siteService = siteServiceImpl;
    }

    public static EmployeeRM toReadMdel(Employee employee) {

        return EmployeeRM.EmployeeRMBuilder.anEmployeeRM()
                .withId(employee.getId())
                .withFirstName(employee.getFirstName())
                .withLastName(employee.getLastName())
                .withMail(employee.getMail())
                .withSiteId(employee.getSiteId())
                .withSiteName(siteService.getSite(employee.getId()).map(Site::getName).orElse(null))
                .build();
    }
}
