package fr.hrboost.api.controller;

import fr.hrboost.api.controller.mapper.SiteMapper;
import fr.hrboost.api.controller.readmodel.SiteRM;
import fr.hrboost.api.model.Country;
import fr.hrboost.api.model.Site;
import fr.hrboost.api.service.SiteService;
import fr.hrboost.api.service.SiteServiceImpl;
import fr.hrboost.api.service.SlackService;
import fr.hrboost.api.service.SlackServiceImpl;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@RequestMapping("/sites")
@RestController
public class SiteController {


    private final SiteService siteService;

    private final SlackService slackService;

    public SiteController(SiteServiceImpl siteServiceImpl, SlackServiceImpl slackServiceImpl) {

        this.siteService = siteServiceImpl;
        this.slackService = slackServiceImpl;
    }

    @Operation(summary = "Get all sites")
    @GetMapping("")
    public ResponseEntity<List<SiteRM>> getSites() {
        Iterable<Site> sites = siteService.getSites();
        List<SiteRM> siteRMs = StreamSupport.stream(sites.spliterator(), false).map(SiteMapper::toReadMdel).collect(Collectors.toList());

        return new ResponseEntity<>(siteRMs, HttpStatus.OK);
    }

    @Operation(summary = "Get one site")
    @GetMapping("/{id}")
    public ResponseEntity<SiteRM> getSite(@Parameter(description = "The id of the site") @PathVariable("id") final Long id) {
        Site site = siteService.getSite(id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Unable to find resource"));

        return new ResponseEntity<>(SiteMapper.toReadMdel(site), HttpStatus.OK);
    }


    @Operation(summary = "Create a new site")
    @PostMapping("")
    public ResponseEntity<Site> createSite(@Parameter(description = "The site object") @RequestBody Site site) {
        Site createdSite = siteService.saveSite(site);
        slackService.sendOpenedMessage(site);
        return new ResponseEntity<>(createdSite, HttpStatus.CREATED);

    }

    @Operation(summary = "Delete a site")
    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteSite(@Parameter(description = "The id of the site") @PathVariable("id") final Long id) {
        Optional<Site> site = siteService.getSite(id);
        if (!site.isPresent()) {
            return new ResponseEntity<>("Site not found", HttpStatus.NOT_FOUND);
        } else {
            siteService.deleteSite(id);
            slackService.sendClosedMessage(site.get());
        }
        return new ResponseEntity<>("Site deleted", HttpStatus.OK);
    }

    @Operation(summary = "Update a site")
    @PutMapping("/{id}")
    public ResponseEntity<Site> updateSite(@Parameter(description = "The id of the site") @PathVariable("id") final Long id,
                                           @Parameter(description = "The site object") @RequestBody Site site) {
        Optional<Site> s = siteService.getSite(id);

        //TODO aligner plustard avec les paramètres finaux d'un employé
        if (s.isPresent()) {
            Site currentSite = s.get();

            Country country = site.getCountry();
            if (country != null) {
                currentSite.setCountry(country);
            }

            String zipCode = site.getZipCode();
            if (zipCode != null) {
                currentSite.setZipCode(zipCode);
            }

            String name = site.getName();
            if (name != null) {
                currentSite.setName(name);
            }

            String city = site.getCity();
            if (city != null) {
                currentSite.setCity(city);
            }

            siteService.saveSite(currentSite);
            return new ResponseEntity<>(currentSite, HttpStatus.OK);
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Unable to find resource");
        }

    }
}
