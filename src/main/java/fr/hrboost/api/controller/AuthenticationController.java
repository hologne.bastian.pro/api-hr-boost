package fr.hrboost.api.controller;

import java.util.Objects;

import fr.hrboost.api.model.jwt.JwtRequest;
import fr.hrboost.api.model.jwt.JwtResponse;
import fr.hrboost.api.filter.util.JwtTokenUtil;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.http.ResponseEntity;

import org.springframework.security.authentication.AuthenticationManager;

import org.springframework.security.authentication.BadCredentialsException;

import org.springframework.security.authentication.DisabledException;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;

import org.springframework.security.core.userdetails.UserDetails;

import org.springframework.security.core.userdetails.UserDetailsService;

import org.springframework.web.bind.annotation.CrossOrigin;

import org.springframework.web.bind.annotation.RequestBody;

import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RequestMethod;

import org.springframework.web.bind.annotation.RestController;



@RestController


@CrossOrigin

public class AuthenticationController {

    @Autowired

    private AuthenticationManager authenticationManager;

    @Autowired

    private JwtTokenUtil jwtTokenUtil;

    @Autowired

    private UserDetailsService jwtInMemoryUserDetailsService;


    @Operation(summary = "Authenticate a user")
    @RequestMapping(value = "/authenticate", method = RequestMethod.POST)

    public ResponseEntity<?> generateAuthenticationToken(@Parameter(description = "The user object") @RequestBody JwtRequest authenticationRequest) throws Exception {

        authenticate(authenticationRequest.getUsername(),

                authenticationRequest.getPassword());


        final UserDetails userDetails = jwtInMemoryUserDetailsService
                .loadUserByUsername(authenticationRequest.getUsername());


        final String token = jwtTokenUtil.generateToken(userDetails);

        System.out.println(token);
        return ResponseEntity.ok(new JwtResponse(token));

    }

    private void authenticate(String username, String password)throws

            Exception {

        Objects.requireNonNull(username);

        Objects.requireNonNull(password);

        try {

            authenticationManager.authenticate(new

                    UsernamePasswordAuthenticationToken(username, password));


        } catch (DisabledException e) {

            throw new Exception("USER_DISABLED", e);

        } catch (BadCredentialsException e) {

            throw new Exception("INVALID_CREDENTIALS", e);
        }
    }
}