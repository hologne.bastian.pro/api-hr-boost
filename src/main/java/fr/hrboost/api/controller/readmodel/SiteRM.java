package fr.hrboost.api.controller.readmodel;

import fr.hrboost.api.model.Country;
import lombok.Data;

@Data
public class SiteRM {
    private Long id;
    private String name;
    private Country country;
    private String city;
    private String zipCode;
    private byte[] image;

    public SiteRM(Long id, String name, Country country, String city, String zipCode, byte[] image) {
        this.id = id;
        this.name = name;
        this.country = country;
        this.city = city;
        this.zipCode = zipCode;
        this.image = image;
    }


    public static final class SiteRMBuilder {
        private Long id;
        private String name;
        private Country country;
        private String city;
        private String zipCode;
        private byte[] image;

        private SiteRMBuilder() {
        }

        public static SiteRMBuilder aSiteRM() {
            return new SiteRMBuilder();
        }

        public SiteRMBuilder withId(Long id) {
            this.id = id;
            return this;
        }

        public SiteRMBuilder withName(String name) {
            this.name = name;
            return this;
        }

        public SiteRMBuilder withCountry(Country country) {
            this.country = country;
            return this;
        }

        public SiteRMBuilder withCity(String city) {
            this.city = city;
            return this;
        }

        public SiteRMBuilder withZipCode(String zipCode) {
            this.zipCode = zipCode;
            return this;
        }

        public SiteRMBuilder withImage(byte[] image) {
            this.image = image;
            return this;
        }

        public SiteRM build() {
            return new SiteRM(id, name, country, city, zipCode, image);
        }
    }
}
