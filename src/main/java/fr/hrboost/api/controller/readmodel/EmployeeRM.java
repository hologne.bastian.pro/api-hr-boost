package fr.hrboost.api.controller.readmodel;

import lombok.Data;

@Data
public class EmployeeRM {
    private Long id;
    private String firstName;
    private String lastName;
    private String mail;
    private Long siteId;
    private String siteName;

    public EmployeeRM(Long id, String firstName, String lastName, String mail, Long siteId, String siteName) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.mail = mail;
        this.siteId = siteId;
        this.siteName = siteName;
    }


    public static final class EmployeeRMBuilder {
        private Long id;
        private String firstName;
        private String lastName;
        private String mail;
        private Long siteId;
        private String siteName;

        private EmployeeRMBuilder() {
        }

        public static EmployeeRMBuilder anEmployeeRM() {
            return new EmployeeRMBuilder();
        }

        public EmployeeRMBuilder withId(Long id) {
            this.id = id;
            return this;
        }

        public EmployeeRMBuilder withFirstName(String firstName) {
            this.firstName = firstName;
            return this;
        }

        public EmployeeRMBuilder withLastName(String lastName) {
            this.lastName = lastName;
            return this;
        }

        public EmployeeRMBuilder withMail(String mail) {
            this.mail = mail;
            return this;
        }

        public EmployeeRMBuilder withSiteId(Long siteId) {
            this.siteId = siteId;
            return this;
        }

        public EmployeeRMBuilder withSiteName(String siteName) {
            this.siteName = siteName;
            return this;
        }

        public EmployeeRM build() {
            return new EmployeeRM(id, firstName, lastName, mail, siteId, siteName);
        }
    }
}
