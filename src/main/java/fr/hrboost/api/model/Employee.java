package fr.hrboost.api.model;

import lombok.Data;

import javax.persistence.*;

/*
Fais le lien entre l'application et la base de données.
 */


@Data // Permet de générer getter/setter automatiquement
@Entity // Precise que cela est une table en BDD
@Table(name = "employees") //Indique le nom de la table associé
public class Employee {

    @Id //Clé primaire
    @GeneratedValue(strategy = GenerationType.IDENTITY) //Auto_Increment
    private Long id;

    @Column(name="first_name") //Fais le lien avec les noms de colonnes de la table car nom pas identique
    private String firstName;

    @Column(name="last_name")
    private String lastName;

    private String mail;

    @Column(name="site_id")
    private Long siteId;

    public void desafecter(){
        this.setSiteId(null);
    }

    public static final class EmployeeBuilder {
        private Long id;
        private String firstName;
        private String lastName;
        private String mail;
        private Long siteId;

        private EmployeeBuilder() {
        }

        public static EmployeeBuilder anEmployee() {
            return new EmployeeBuilder();
        }


        public EmployeeBuilder withFirstName(String firstName) {
            this.firstName = firstName;
            return this;
        }

        public EmployeeBuilder withLastName(String lastName) {
            this.lastName = lastName;
            return this;
        }

        public EmployeeBuilder withMail(String mail) {
            this.mail = mail;
            return this;
        }

        public EmployeeBuilder withSiteId(Long siteId) {
            this.siteId = siteId;
            return this;
        }

        public Employee build() {
            Employee employee = new Employee();
            employee.setId(id);
            employee.setFirstName(firstName);
            employee.setLastName(lastName);
            employee.setMail(mail);
            employee.setSiteId(siteId);
            return employee;
        }
    }
}
