package fr.hrboost.api.model;

import lombok.Data;

import javax.persistence.*;

@Data // Permet de générer getter/setter automatiquement
@Entity // Precise que cela est une table en BDD
@Table(name = "sites") //Indique le nom de la table associé
public class Site {

    @Id //Clé primaire
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    @Enumerated(EnumType.STRING)
    private Country country;

    private String city;

    @Enumerated(EnumType.STRING)
    private Image image;

    @Column(name="zip_code")
    private String zipCode;


}
