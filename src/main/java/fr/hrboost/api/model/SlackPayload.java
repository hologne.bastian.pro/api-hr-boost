package fr.hrboost.api.model;

public class SlackPayload {
    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    private String text;

    public SlackPayload(String text) {
        this.text = text;
    }

    //Getter, Setter and Constructor left for brevity
}
