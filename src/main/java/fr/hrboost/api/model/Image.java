package fr.hrboost.api.model;

public enum Image {
    HR1("hr1.jpg"),
    HR2("hr2.jpg"),
    HR3("hr3.jpg"),
    HR4("hr4.jpg"),
    HR5("hr5.jpg"),
    INDUSTRY1("industry1.jpg"),
    INDUSTRY2("industry2.jpg"),
    INDUSTRY3("industry3.jpg"),
    INDUSTRY4("industry4.jpg"),
    INDUSTRY5("industry5.jpg"),
    INDUSTRY6("industry6.jpg"),
    INDUSTRY7("industry7.jpg"),
    IT1("it1.jpg"),
    IT2("it2.jpg"),
    IT3("it3.jpg"),
    IT4("it4.jpg"),
    DEFAULT("default.png");

    private String name;

    public String getName() {
        return name;
    }

    Image(String name) {
        this.name = name;
    }

}
