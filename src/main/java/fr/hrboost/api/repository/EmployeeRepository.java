package fr.hrboost.api.repository;

import fr.hrboost.api.model.Employee;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;


/** Ça simple implémentation binder avec Employée permet de
  bénéficier de plusieurs des methodes crud sur l'entité Employé.
  Doc : https://docs.spring.io/spring-data/commons/docs/current/api/org/springframework/data/repository/CrudRepository.html
 */
@Repository
public interface EmployeeRepository extends PagingAndSortingRepository<Employee, Long> {

    Page<Employee> findAllBySiteId(long siteId, Pageable pageable);
    Page<Employee> findAll(Pageable pageable);


}
