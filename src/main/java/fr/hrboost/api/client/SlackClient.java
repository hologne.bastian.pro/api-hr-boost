package fr.hrboost.api.client;

import fr.hrboost.api.model.SlackPayload;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class SlackClient {

    private final String slackWebHookUrl = "https://hooks.slack.com/services/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";
    private final RestTemplate restTemplate;

    @Autowired
    public SlackClient(RestTemplateBuilder restTemplateBuilder) {
        this.restTemplate = restTemplateBuilder.build();
    }

    public String sendMessage(String text) {

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        SlackPayload payload = new SlackPayload(text);
        HttpEntity<SlackPayload> request = new HttpEntity<>(payload, headers);

        String response = null;


        try{
             response = restTemplate.exchange(
                    slackWebHookUrl,
                    HttpMethod.POST,
                    request,
                    String.class).getBody();
        }catch (Exception e){
            System.out.println("Le service slack est momentanéement indisponible");
        }


        return response;
    }
}