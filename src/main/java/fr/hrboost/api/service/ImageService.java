package fr.hrboost.api.service;

import fr.hrboost.api.model.Image;

public interface ImageService {
    public byte[] getImage(Image image);
}
