package fr.hrboost.api.service;

import fr.hrboost.api.model.Employee;
import fr.hrboost.api.model.Site;

public interface SlackService {

    public void sendWelcomeMessage(Employee employee);

    public void sendDismissalMessage(Employee employee);
    /*
     * Slack messages about Sites
     * */
    public void sendOpenedMessage(Site site);

    public void sendClosedMessage(Site site);
}
