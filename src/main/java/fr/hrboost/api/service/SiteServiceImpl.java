package fr.hrboost.api.service;

import fr.hrboost.api.model.Site;
import fr.hrboost.api.repository.SiteRepository;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.stream.StreamSupport;

@Service
public class SiteServiceImpl implements SiteService {

    private final SiteRepository siteRepository;

    private final EmployeeService employeeService;

    public SiteServiceImpl(SiteRepository siteRepository, EmployeeServiceImpl employeeServiceImpl) {
        this.siteRepository = siteRepository;
        this.employeeService = employeeServiceImpl;
    }

    public Iterable<Site> getSites(){
        return siteRepository.findAll();
    }

    public Optional<Site> getSite(Long id){
        return siteRepository.findById(id);
    }

    public Site saveSite(Site site){
        return siteRepository.save(site);
    }

    public void deleteSite(final Long id){

        StreamSupport.stream(employeeService.getEmployees().spliterator(), false)
                        .filter(employee -> employee.getSiteId() != null &&  employee.getSiteId().equals(id))
                        .forEach(employee -> {
                            employee.desafecter();
                            employeeService.saveEmployee(employee);
                        });

        siteRepository.deleteById(id);
    }


}
