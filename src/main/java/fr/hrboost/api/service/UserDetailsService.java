package fr.hrboost.api.service;

import java.util.ArrayList;

        import org.springframework.security.core.userdetails.User;

        import org.springframework.security.core.userdetails.UserDetails;

import org.springframework.security.core.userdetails.UsernameNotFoundException;

import org.springframework.stereotype.Service;


@Service

public class UserDetailsService implements org.springframework.security.core.userdetails.UserDetailsService {

    @Override

    public UserDetails loadUserByUsername(String username) throws

            UsernameNotFoundException {
        //Un seul user actuellement et il est en dur.
        if ("springuser".equals(username)) {

            return new User("springuser",

                    "$2a$10$ixlPY3AAd4ty1l6E2IsQ9OFZi2ba9ZQE0bP7RFcGIWNhyFrrT3YUi",
                    new ArrayList<>());

        } else {

            throw new UsernameNotFoundException("User not found with username: " + username);

        }
    }

}