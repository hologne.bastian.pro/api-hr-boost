package fr.hrboost.api.service;

import fr.hrboost.api.model.Employee;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

public interface EmployeeService {

    public Optional<Employee> getEmployee(final Long id);

    public Iterable<Employee> getEmployees();

    public Page<Employee> getEmployees(Pageable pageable);
    public Page<Employee> getEmployeesBySite(Long siteId,Pageable pageable);


    public void deleteEmployee(final Long id);

    public Employee saveEmployee(Employee employee);
}
