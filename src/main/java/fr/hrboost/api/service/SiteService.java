package fr.hrboost.api.service;

import fr.hrboost.api.model.Site;

import java.util.Optional;

public interface SiteService {


    public Iterable<Site> getSites();

    public Optional<Site> getSite(Long id);

    public Site saveSite(Site site);

    public void deleteSite(final Long id);
}
