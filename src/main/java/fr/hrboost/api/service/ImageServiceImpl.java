package fr.hrboost.api.service;

import fr.hrboost.api.model.Image;
import org.apache.commons.io.IOUtils;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.InputStream;
import java.util.Optional;

@Component
public class ImageServiceImpl implements ImageService{

    @Override
    public byte[] getImage(Image image)  {

        InputStream in = getClass()
                .getResourceAsStream("/image/" + Optional.ofNullable(image).orElse(Image.DEFAULT).getName());

        try {
            return IOUtils.toByteArray(in);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }





}
