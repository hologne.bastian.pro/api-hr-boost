package fr.hrboost.api.service;

import fr.hrboost.api.client.SlackClient;
import fr.hrboost.api.model.Employee;
import fr.hrboost.api.model.Site;
import org.springframework.stereotype.Component;

@Component
public class SlackServiceImpl implements SlackService {

   private final SlackClient slackClient;

    public SlackServiceImpl(SlackClient slackClient) {
        this.slackClient = slackClient;
    }

    /*
    * Slack messages about Employees
    * */
    public void sendWelcomeMessage(Employee employee) {
        final String welcomeMessage = "*[RECRUTEMENT]* " + employee.getFirstName() + " " + employee.getLastName() + " rejoint le groupe BL";
        slackClient.sendMessage(welcomeMessage);
    }

    public void sendDismissalMessage(Employee employee) {
        final String welcomeMessage = "*[LICENCIEMENT]* " + employee.getFirstName() + " " + employee.getLastName() + "a été licencié du groupe BL";
        slackClient.sendMessage(welcomeMessage);
    }
    /*
     * Slack messages about Sites
     * */
    public void sendOpenedMessage(Site site) {
        final String openedServiceMessage = " *[OUVERTURE DE SITE]* Le site " + site.getName() + " a été ouvert ! Bienvenue !";
        slackClient.sendMessage(openedServiceMessage);
    }

    public void sendClosedMessage(Site site) {
        final String openedServiceMessage = " *[FERMETURE DE SITE]* Le site " + site.getName() + " a été fermé !";
        slackClient.sendMessage(openedServiceMessage);
    }
}
