package fr.hrboost.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class ApiApplication {

	private static ApplicationContext appContext;
	public static void main(String[] args) {
		SpringApplication.run(ApiApplication.class, args);
	}

	public static ApplicationContext getAppContext() {
		return appContext;
	}

}
